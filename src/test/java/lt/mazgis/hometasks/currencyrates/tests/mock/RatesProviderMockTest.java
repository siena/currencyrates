package lt.mazgis.hometasks.currencyrates.tests.mock;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import lt.mazgis.hometasks.currencyrates.ratesprovider.RateProviderLBFxRatesAdapter;
import lt.mazgis.hometasks.currencyrates.ratesprovider.ServiceProviderException;
import lt.mazgis.hometasks.currencyrates.ratesprovider.dao.CcyRateChange;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.LBFxRatesClient;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.CcyAmtHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.CcyISO4217;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.FxRateHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.FxRateTypeHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.ext.CcyNm;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.ext.CcyNtry;

@RunWith(MockitoJUnitRunner.class)
public class RatesProviderMockTest {

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @Mock
    LBFxRatesClient ratesClient;

    @InjectMocks
    RateProviderLBFxRatesAdapter ratesProvider;

    @Test
    public void testDataConvertion() throws DatatypeConfigurationException, ServiceProviderException {
	when(this.ratesClient.getCurrencyList()).thenReturn(this.getCurrencyType());
	when(this.ratesClient.getFxRates(argThat(m -> m.equals(FxRateTypeHandling.EU)), argThat(m -> true))).thenReturn(this.getFxRateValue());
	final Collection<CcyRateChange> responce = this.ratesProvider.getRateChanges(LocalDate.of(2019, 1, 20), LocalDate.of(2019, 1, 1));
	verify(this.ratesClient, times(1)).getCurrencyList();
	verify(this.ratesClient, times(1)).getFxRates(argThat(m -> m.equals(FxRateTypeHandling.EU)),
		argThat(m -> m.isEqual(LocalDate.of(2019, 1, 20))));
	verify(this.ratesClient, times(1)).getFxRates(argThat(m -> m.equals(FxRateTypeHandling.EU)),
		argThat(m -> m.isEqual(LocalDate.of(2019, 1, 1))));
	assertNotNull(responce);
	assertTrue(responce.size() == 1);
	responce.forEach(r -> {
	    assertNotNull(r);
	    assertTrue(this.formatter.format(r.getDateStart()).equals("2019-01-20"));
	    assertTrue(this.formatter.format(r.getDateEnd()).equals("2019-01-20"));
	    assertTrue(r.getStart().toString().equals("19.54805"));
	    assertTrue(r.getEnd().toString().equals("19.54805"));
	    assertNotNull(r.getCcy());
	    assertTrue(r.getCcy().getAccr().equals("MDL"));
	    assertTrue(r.getCcy().getName().size() == 2);
	    r.getCcy().getName().forEach((k, v) -> {
		assertTrue((k.endsWith("LT") && v.endsWith("Moldovos lėja")) || (k.endsWith("EN") && v.endsWith("Moldovan leu")));
	    });
	});

    }

    private List<CcyNtry> getCurrencyType() {
	return (Arrays
		.asList(new CcyNtry(CcyISO4217.MDL, "498", "2", Arrays.asList(new CcyNm("LT", "Moldovos lėja"), new CcyNm("EN", "Moldovan leu")))));
    }

    private List<FxRateHandling> getFxRateValue() throws DatatypeConfigurationException {
	return Arrays.asList(this.getFxRate(FxRateTypeHandling.EU, "2019-01-20"));
    }

    private FxRateHandling getFxRate(final FxRateTypeHandling tp, final String date) throws DatatypeConfigurationException {
	final FxRateHandling rate = new FxRateHandling();
	rate.setTp(tp);
	rate.setDt(DatatypeFactory.newInstance().newXMLGregorianCalendar(date));
	rate.getCcyAmt()
	        .addAll(Arrays.asList(this.getCcyamt(CcyISO4217.EUR, BigDecimal.ONE), this.getCcyamt(CcyISO4217.MDL, BigDecimal.valueOf(19.54805))));
	return rate;
    }

    private CcyAmtHandling getCcyamt(final CcyISO4217 ccy, final BigDecimal v) {
	final CcyAmtHandling ccyAmt = new CcyAmtHandling();
	ccyAmt.setCcy(ccy);
	ccyAmt.setAmt(v);
	return ccyAmt;
    }
}
