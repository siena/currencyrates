package lt.mazgis.hometasks.currencyrates.tests;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import lt.mazgis.hometasks.currencyrates.ratesprovider.CurrencyRatesProvider;
import lt.mazgis.hometasks.currencyrates.ratesprovider.ServiceProviderException;
import lt.mazgis.hometasks.currencyrates.ratesprovider.dao.CcyRateChange;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { CurrencyRatesTestConfigurator.class })
@TestPropertySource(locations = "classpath:test-config.properties")
public class CurrencyRatesProviderTests {

    @Autowired(required = true)
    @Qualifier("CurrencyRatesProvider")
    private CurrencyRatesProvider provider;

    @Test
    public void testRatesWithCorrectDates() throws ServiceProviderException {
	final Collection<CcyRateChange> rateChanges = this.provider.getRateChanges(LocalDate.now(), LocalDate.now().minusWeeks(2));
	assertNotNull(rateChanges);
	assertFalse(rateChanges.isEmpty());
	this.testOrder(rateChanges);
    }

    @Test // For strange null pointer exception before 2015
    public void testRatesWithCorrectDatesBefore2015() throws ServiceProviderException {
	final Collection<CcyRateChange> rateChanges = this.provider.getRateChanges(LocalDate.now(), LocalDate.of(2014, 12, 31));
	assertNotNull(rateChanges);
	assertFalse(rateChanges.isEmpty());
    }

    @Test(expected = ServiceProviderException.class)
    public void testRatesWithInCorrectDatesEndBeforeStart() throws ServiceProviderException {
	this.provider.getRateChanges(LocalDate.now().minusWeeks(2), LocalDate.now());
    }

    @Test(expected = ServiceProviderException.class)
    public void testRatesWithInCorrectDatesStartBefore20141001() throws ServiceProviderException {
	final Collection<CcyRateChange> rateChanges = this.provider.getRateChanges(LocalDate.now(), LocalDate.of(2014, 9, 29));
	assertNotNull(rateChanges);
	assertFalse(rateChanges.isEmpty());
    }

    private void testOrder(final Collection<CcyRateChange> rateChanges) {
	CcyRateChange crPrev = null;
	for (final CcyRateChange cr : rateChanges) {
	    if (crPrev != null) {
		assertTrue(cr.getChangePercent().compareTo(crPrev.getChangePercent()) <= 0);
	    }
	    crPrev = cr;
	}
    }
}
