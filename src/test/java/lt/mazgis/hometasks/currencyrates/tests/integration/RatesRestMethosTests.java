package lt.mazgis.hometasks.currencyrates.tests.integration;

import static org.junit.Assert.*;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import lt.mazgis.hometasks.currencyrates.CurrencyRatesConfigurator;
import lt.mazgis.hometasks.currencyrates.ratesprovider.dao.CcyRateChange;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { CurrencyRatesConfigurator.class })
@TestPropertySource(locations = "classpath:test-config.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RatesRestMethosTests {

    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
	this.base = new URL("http://localhost:" + this.port + "/api/changes/2019-01-20:2019-01-23");
    }

    @Test
    public void testService() {
	final ResponseEntity<CcyRateChange[]> response = this.template.getForEntity(this.base.toString(), CcyRateChange[].class);
	assertNotNull(response);
	assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	assertNotNull(response.getBody());
	assertTrue(response.getBody().length > 0);
	for (final CcyRateChange cr : response.getBody()) {
	    assertNotNull(cr.getStart());
	    assertNotNull(cr.getEnd());
	    assertNotNull(cr.getDateEnd());
	    assertNotNull(cr.getDateStart());
	    assertNotNull(cr.getCcy());
	    assertNotNull(cr.getCcy().getAccr());
	    assertNotNull(cr.getCcy().getName());
	    assertTrue(cr.getCcy().getName().size() >= 2);
	    cr.getCcy().getName().forEach((k, v) -> {
		assertTrue(k.equals("LT") || k.equals("EN"));
		assertFalse(v.isEmpty());
	    });
	}
    }

}
