package lt.mazgis.hometasks.currencyrates.tests;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.LBFxRatesClient;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.LBFxRatesException;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.FxRateHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.FxRateTypeHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.ext.CcyNtry;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { CurrencyRatesTestConfigurator.class })
@TestPropertySource(locations = "classpath:test-config.properties")
public class LBFxRatesSoapMethosTests {

    @Autowired(required = true)
    private LBFxRatesClient ratesClient;

    @Test
    public void testGetCurrentFxRates() throws LBFxRatesException {
	final Collection<FxRateHandling> response = this.ratesClient.getCurrentFxRates(FxRateTypeHandling.EU);
	assertNotNull(response);
	assertTrue(response.size() > 0);
	response.forEach(r -> {
	    assertNotNull(r.getTp());
	    assertNotNull(r.getDt());
	    assertNotNull(r.getCcyAmt());
	    assertTrue(r.getCcyAmt().size() == 2);
	    r.getCcyAmt().forEach(ccyamt -> {
		assertNotNull(ccyamt.getAmt());
		assertNotNull(ccyamt.getCcy());
	    });
	});
    }

    @Test
    public void testGetCurrencyList() throws LBFxRatesException {
	final Collection<CcyNtry> response = this.ratesClient.getCurrencyList();
	assertNotNull(response);
	assertTrue(response.size() > 0);
	response.forEach(ccy -> {
	    assertNotNull(ccy.getCcy());
	    assertNotNull(ccy.getMnrUnts());
	    assertNotNull(ccy.getNr());
	    assertNotNull(ccy.getNames());
	    assertTrue(ccy.getNames().size() == 2);
	    ccy.getNames().forEach(ccynm -> {
		assertNotNull(ccynm.getLang());
		assertNotNull(ccynm.getName());
	    });
	});
    }
}
