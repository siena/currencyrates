package lt.mazgis.hometasks.currencyrates.rest;

import java.time.LocalDate;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lt.mazgis.hometasks.currencyrates.ratesprovider.CurrencyRatesProvider;
import lt.mazgis.hometasks.currencyrates.ratesprovider.ServiceProviderException;
import lt.mazgis.hometasks.currencyrates.ratesprovider.dao.CcyRateChange;

@RestController
@RequestMapping("/api")
public class CurrencyRatesREST {

    @Autowired
    @Qualifier("CurrencyRatesProvider")
    private CurrencyRatesProvider ratesProvider;

    @RequestMapping(path = "/changes/{start}:{end}", method = RequestMethod.GET, produces = "application/json")
    public Collection<CcyRateChange> ratesChanges(@PathVariable(name = "start") @DateTimeFormat(iso = ISO.DATE) final LocalDate start,
	    @PathVariable(name = "end") @DateTimeFormat(iso = ISO.DATE) final LocalDate end) {
	Collection<CcyRateChange> rates;
	try {
	    rates = this.ratesProvider.getRateChanges(end, start);
	} catch (final ServiceProviderException e) {
	    throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
	}
	return rates;
    }

    public void setRatesProvider(final CurrencyRatesProvider ratesProvider) {
	this.ratesProvider = ratesProvider;
    }

}
