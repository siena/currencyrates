package lt.mazgis.hometasks.currencyrates.view;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class PageRedirect implements WebMvcConfigurer {

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
	registry.addViewController("/")
	        .setViewName("forward:/ratechange.xhtml");
	registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }
}