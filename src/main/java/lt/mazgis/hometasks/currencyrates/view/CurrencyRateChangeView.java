package lt.mazgis.hometasks.currencyrates.view;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import lt.mazgis.hometasks.currencyrates.ratesprovider.CurrencyRatesProvider;
import lt.mazgis.hometasks.currencyrates.ratesprovider.ServiceProviderException;
import lt.mazgis.hometasks.currencyrates.ratesprovider.dao.CcyRateChange;

@Named
@ViewScoped
public class CurrencyRateChangeView {

    @Autowired
    @Qualifier("CurrencyRatesProvider")
    private CurrencyRatesProvider provider;

    private Date endDate = new Date();
    private Date startDate = new Date();

    private ServiceProviderException providerException;

    private final List<CcyRateChange> rateChanges = new ArrayList<CcyRateChange>();

    public Date getEndDate() {
	return this.endDate;
    }

    public Date getStartDate() {
	return this.startDate;
    }

    public void setEndDate(final Date endDate) {
	this.endDate = endDate;
    }

    public void setStartDate(final Date startDate) {
	this.startDate = startDate;
    }

    public Date getMaxDate() {
	return new Date();
    }

    public void onSubmit() {
	try {
	    this.rateChanges.clear();
	    this.providerException = null;
	    this.rateChanges.addAll(this.provider.getRateChanges(this.endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
	            this.startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
	} catch (final ServiceProviderException e) {
	    this.providerException = e;
	}
    }

    public boolean isError() {
	return this.providerException != null;
    }

    public Collection<CcyRateChange> getRatesChange() {
	return this.rateChanges;
    }
}