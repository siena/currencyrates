package lt.mazgis.hometasks.currencyrates.ratesprovider;

public class ServiceProviderException extends Exception {

    public ServiceProviderException(final String message) {
	super(message);
    }

    public ServiceProviderException(final String message,final Throwable cause) {
	super(message, cause);
    }

}
