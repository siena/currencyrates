package lt.mazgis.hometasks.currencyrates.ratesprovider.lb;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.springframework.util.Assert;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.FxRateHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.FxRateTypeHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.FxRatesHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.GetCurrencyList;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.GetCurrencyListResponse;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.GetCurrentFxRates;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.GetCurrentFxRatesResponse;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.GetFxRates;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.GetFxRatesResponse;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.OprlErrHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.ext.CcyNtry;

public class LBFxRatesClient extends WebServiceGatewaySupport {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public LBFxRatesClient(final String uri) throws Exception {
	try {
	    URI.create(uri);
	} catch (final IllegalArgumentException e) {
	    throw new Exception("Incorrct URI provided : " + uri, e);
	}
	this.setDefaultUri(uri);
    }

    public List<CcyNtry> getCurrencyList() throws LBFxRatesException{
	final GetCurrencyList request = new GetCurrencyList();
	final GetCurrencyListResponse response = (GetCurrencyListResponse) this.getWebServiceTemplate().marshalSendAndReceive(request,
		new SoapActionCallback("http://www.lb.lt/WebServices/FxRates/getCurrencyList"));
	try {
	    Assert.notNull(response.getGetCurrencyListResult(), "getCurrencyList responce has not value");
	    Assert.notNull(response.getGetCurrencyListResult().getContent(), "getCurrencyList responce value haz no content");
	    Assert.notEmpty(response.getGetCurrencyListResult().getContent(), "getCurrencyList responce value content is empty");
	    Assert.isInstanceOf(JAXBElement.class, response.getGetCurrencyListResult().getContent().get(0),
		    "getCurrencyList responce value content incorrect type");
	}catch (final RuntimeException e) {
	    throw new LBFxRatesException(e.getMessage(),e);
	}
	final FxRatesHandling result = ((JAXBElement<FxRatesHandling>) response.getGetCurrencyListResult().getContent().get(0)).getValue();
	this.checkForError(result.getOprlErr());
	return result.getCcyNtry();
    }

    public List<FxRateHandling> getFxRates(final FxRateTypeHandling tp, final LocalDate dt) throws LBFxRatesException {
	final GetFxRates request = new GetFxRates();
	request.setTp(tp.name());
	request.setDt(dt.format(this.formatter));
	final GetFxRatesResponse response = (GetFxRatesResponse) this.getWebServiceTemplate().marshalSendAndReceive(request,
		new SoapActionCallback("http://www.lb.lt/WebServices/FxRates/getFxRates"));
	try {
	    Assert.notNull(response.getGetFxRatesResult(), "getFxRates responce has not value");
	    Assert.notNull(response.getGetFxRatesResult().getContent(), "getFxRates responce value haz no content");
	    Assert.notEmpty(response.getGetFxRatesResult().getContent(), "getFxRates responce value content is empty");
	    Assert.isInstanceOf(JAXBElement.class, response.getGetFxRatesResult().getContent().get(0),
		    "getCurrencyList responce value content incorrect type");
	} catch (final RuntimeException e) {
	    throw new LBFxRatesException(e.getMessage(), e);
	}
	final FxRatesHandling result = ((JAXBElement<FxRatesHandling>) response.getGetFxRatesResult().getContent().get(0)).getValue();
	this.checkForError(result.getOprlErr());
	return result.getFxRate();
    }

    public List<FxRateHandling> getCurrentFxRates(final FxRateTypeHandling tp) throws LBFxRatesException {
	final GetCurrentFxRates request = new GetCurrentFxRates();
	request.setTp(tp.name());
	final GetCurrentFxRatesResponse response = (GetCurrentFxRatesResponse) this.getWebServiceTemplate().marshalSendAndReceive(request,
		new SoapActionCallback("http://www.lb.lt/WebServices/FxRates/getCurrentFxRates"));
	try {
	    Assert.notNull(response.getGetCurrentFxRatesResult(), "getCurrentFxRates responce has not value");
	    Assert.notNull(response.getGetCurrentFxRatesResult().getContent(), "getCurrentFxRates responce value haz no content");
	    Assert.notEmpty(response.getGetCurrentFxRatesResult().getContent(), "getCurrentFxRates responce value content is empty");
	    Assert.isInstanceOf(JAXBElement.class, response.getGetCurrentFxRatesResult().getContent().get(0),
		    "getCurrentFxRates responce value content incorrect type");
	} catch (final RuntimeException e) {
	    throw new LBFxRatesException(e.getMessage(), e);
	}
	final FxRatesHandling result = ((JAXBElement<FxRatesHandling>) response.getGetCurrentFxRatesResult().getContent().get(0)).getValue();
	this.checkForError(result.getOprlErr());
	return result.getFxRate();
    }

    private void checkForError(final OprlErrHandling error) throws LBFxRatesException {
	if (error != null) {
	    throw new LBFxRatesException(error.getDesc(), error.getErr().getPrtry());
	}
    }

}
