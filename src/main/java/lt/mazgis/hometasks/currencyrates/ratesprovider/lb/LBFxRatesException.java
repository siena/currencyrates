package lt.mazgis.hometasks.currencyrates.ratesprovider.lb;

import lt.mazgis.hometasks.currencyrates.ratesprovider.ServiceProviderException;

public class LBFxRatesException extends ServiceProviderException {

    private final String errorCode;


    public LBFxRatesException(final String message, final Throwable cause) {
	super(message, cause);
	this.errorCode = "";
    }

    public LBFxRatesException(final String message) {
	super(message);
	this.errorCode="";
    }

    public LBFxRatesException(final String message, final String errorCode) {
	super(message);
	this.errorCode = errorCode;

    }

    public String getErrorCode() {
	return this.errorCode;
    }
}
