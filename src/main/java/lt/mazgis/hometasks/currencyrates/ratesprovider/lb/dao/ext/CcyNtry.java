package lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.ext;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.CcyISO4217;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CcyNtry")
public class CcyNtry {

    @XmlElement(name="Ccy")
    private CcyISO4217 ccy;

    @XmlElement(type=CcyNm.class,name="CcyNm")
    private Collection<CcyNm> names;

    @XmlElement(name="CcyNbr")
    private String nr;

    @XmlElement(name="CcyMnrUnts")
    private String mnrUnts;

    public CcyNtry() {
    }

    public CcyNtry(final CcyISO4217 acronym, final String nr, final String mnrUnts, final Collection<CcyNm> names) {
	super();
	this.ccy = acronym;
	this.names = names;
	this.nr = nr;
	this.mnrUnts = mnrUnts;
    }

    public CcyISO4217 getCcy() {
	return this.ccy;
    }

    public String getMnrUnts() {
	return this.mnrUnts;
    }
    public Collection<CcyNm> getNames() {
	return this.names;
    }
    public String getNr() {
	return this.nr;
    }

}
