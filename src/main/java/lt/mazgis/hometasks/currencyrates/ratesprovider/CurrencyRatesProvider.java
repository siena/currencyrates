package lt.mazgis.hometasks.currencyrates.ratesprovider;

import java.time.LocalDate;
import java.util.Collection;

import lt.mazgis.hometasks.currencyrates.ratesprovider.dao.CcyRateChange;

public interface CurrencyRatesProvider {

    Collection<CcyRateChange> getRateChanges(LocalDate end, LocalDate start) throws ServiceProviderException;

}