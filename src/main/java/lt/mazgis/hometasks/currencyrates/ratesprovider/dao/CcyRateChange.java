package lt.mazgis.hometasks.currencyrates.ratesprovider.dao;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class CcyRateChange implements Comparable<CcyRateChange> {

    private BigDecimal end;
    private BigDecimal start;
    private Date dateEnd;
    private Date dateStart;
    private Ccy ccy;

    public BigDecimal getEnd() {
	return this.end;
    }

    public void setEnd(final BigDecimal end) {
	this.end = end;
    }

    public BigDecimal getStart() {
	return this.start;
    }

    public void setStart(final BigDecimal start) {
	this.start = start;
    }

    public Date getDateEnd() {
	return this.dateEnd;
    }

    public void setDateEnd(final Date dateEnd) {
	this.dateEnd = dateEnd;
    }

    public Date getDateStart() {
	return this.dateStart;
    }

    public void setDateStart(final Date dateStart) {
	this.dateStart = dateStart;
    }

    public Ccy getCcy() {
	return this.ccy;
    }

    public void setCcy(final Ccy ccy) {
	this.ccy = ccy;
    }

    public BigDecimal getChangePercent() {
	if ((this.end == null) || (this.start == null)) {
	    return BigDecimal.valueOf(-1000);
	}
	final BigDecimal diff = this.end.subtract(this.start);
	return diff.divide(this.end, 3, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
    }

    public boolean isValid() {
	return (this.end != null) && (this.start != null);
    }

    @Override
    public int compareTo(final CcyRateChange o) {
	final int compate = o.getChangePercent().compareTo(this.getChangePercent());
	return compate != 0 ? compate : this.getCcy().getAccr().compareToIgnoreCase(o.getCcy().getAccr());
    }
}
