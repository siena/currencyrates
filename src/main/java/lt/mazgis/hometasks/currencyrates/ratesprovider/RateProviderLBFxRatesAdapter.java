package lt.mazgis.hometasks.currencyrates.ratesprovider;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import lt.mazgis.hometasks.currencyrates.ratesprovider.dao.Ccy;
import lt.mazgis.hometasks.currencyrates.ratesprovider.dao.CcyRateChange;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.LBFxRatesClient;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.CcyAmtHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.CcyISO4217;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.FxRateHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.FxRateTypeHandling;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.ext.CcyNm;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.ext.CcyNtry;

public class RateProviderLBFxRatesAdapter implements CurrencyRatesProvider {

    private final LBFxRatesClient fxRatesClient;

    SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-dd-MM");

    public RateProviderLBFxRatesAdapter(final LBFxRatesClient fxRatesClient) {
	this.fxRatesClient = fxRatesClient;
    }

    @Override
    public Collection<CcyRateChange> getRateChanges(final LocalDate end, final LocalDate start) throws ServiceProviderException {
	if (end.isBefore(start)) {
	    throw new ServiceProviderException("Incorrect dates privided End date should aftes start date");
	}
	final List<CcyNtry> curencyList = this.fxRatesClient.getCurrencyList();
	// Convert an map by currency accronym
	final Map<String, CcyRateChange> ccyRatesCahanges = curencyList.stream().map(ccy -> {
	    final CcyRateChange cr = new CcyRateChange();
	    final Ccy c = new Ccy();
	    c.setAccr(ccy.getCcy().name());
	    c.setName(ccy.getNames().stream().collect(Collectors.toMap(CcyNm::getLang, CcyNm::getName)));
	    cr.setCcy(c);
	    return cr;
	}).collect(Collectors.toMap(cr -> cr.getCcy().getAccr(), Function.identity()));

	final List<FxRateHandling> ratesEnd = this.fxRatesClient.getFxRates(FxRateTypeHandling.EU, end);
	final List<FxRateHandling> ratesStart = this.fxRatesClient.getFxRates(FxRateTypeHandling.EU, start);

	// set rates for End
	ratesEnd.forEach(r -> {
	    final Optional<CcyAmtHandling> amount = r.getCcyAmt().stream().filter(a -> !a.getCcy().equals(CcyISO4217.EUR)).findAny();
	    amount.ifPresent(a -> {
		ccyRatesCahanges.computeIfPresent(a.getCcy().name(), (k, v) -> {
		    v.setDateEnd(r.getDt().toGregorianCalendar().getTime());
		    v.setEnd(a.getAmt());
		    return v;
		});
	    });
	});
	// set rates for Start
	ratesStart.forEach(r -> {
	    // Strange null pointer then date is before 2015
	    final Optional<CcyAmtHandling> amount = r.getCcyAmt().stream().filter(a -> ((a.getCcy() != null) && !a.getCcy().equals(CcyISO4217.EUR)))
		    .findAny();
	    amount.ifPresent(a -> {
		ccyRatesCahanges.computeIfPresent(a.getCcy().name(), (k, v) -> {
		    v.setDateStart(r.getDt().toGregorianCalendar().getTime());
		    v.setStart(a.getAmt());
		    return v;
		});
	    });
	});
	return ccyRatesCahanges.values().stream().filter(c -> c.isValid()).sorted().collect(Collectors.toList());
    }

}
