package lt.mazgis.hometasks.currencyrates.ratesprovider.dao;

import java.math.BigDecimal;
import java.util.Date;

public class CcyRate {

    private BigDecimal rate;
    private Date date;
    private Ccy ccy;



    public BigDecimal getRate() {
	return this.rate;
    }

    public void setRate(final BigDecimal rate) {
	this.rate = rate;
    }

    public Date getDate() {
	return this.date;
    }

    public void setDate(final Date date) {
	this.date = date;
    }

    public Ccy getCcy() {
	return this.ccy;
    }

    public void setCcy(final Ccy ccy) {
	this.ccy = ccy;
    }

    public boolean isValid() {
	return (this.rate != null) && (this.date != null);
    }
}
