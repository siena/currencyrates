package lt.mazgis.hometasks.currencyrates.ratesprovider.dao;

import java.util.Map;

public class Ccy {

    private String accr;
    private Map<String, String> name;

    public String getAccr() {
	return this.accr;
    }

    public void setAccr(final String accr) {
	this.accr = accr;
    }

    public Map<String, String> getName() {
	return this.name;
    }

    public void setName(final Map<String, String> name) {
	this.name = name;
    }

}
