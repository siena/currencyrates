package lt.mazgis.hometasks.currencyrates;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import lt.mazgis.hometasks.currencyrates.ratesprovider.CurrencyRatesProvider;
import lt.mazgis.hometasks.currencyrates.ratesprovider.RateProviderLBFxRatesAdapter;
import lt.mazgis.hometasks.currencyrates.ratesprovider.lb.LBFxRatesClient;

@SpringBootApplication
@PropertySource("classpath:config.properties")
public class CurrencyRatesConfigurator {

    @Value("${lb.fxrate.uri}")
    private String lbFxRateServiceURI;

    public static void main(final String[] args) {
	SpringApplication.run(CurrencyRatesConfigurator.class, args);
    }

    @Bean
    public Jaxb2Marshaller marshaller() {
	final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
	marshaller.setContextPaths("lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao",
		"lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao.ext");
	return marshaller;
    }

    @Bean
    public LBFxRatesClient lbFxRatesClient(final Jaxb2Marshaller marshaller) throws Exception {
	final LBFxRatesClient client = new LBFxRatesClient(this.lbFxRateServiceURI);
	client.setMarshaller(marshaller);
	client.setUnmarshaller(marshaller);
	return client;
    }

    @Bean(name = "CurrencyRatesProvider")
    public CurrencyRatesProvider currencyRatesProvider(final LBFxRatesClient fxRatesClient) {
	return new RateProviderLBFxRatesAdapter(fxRatesClient);
    }

}
