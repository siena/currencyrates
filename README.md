# LBFxRatesSOAP2REST

Lithuanian Bank currency rates SOAP service bridge to REST Service

To compile run command:
	
	mvn clean package -Dhttp.agent=any

After procject successfully build to start application run command:

	mvn spring-boot:run -Dhttp.agent=any
or

	java  -jar target/currencyrates-0.0.1-SNAPSHOT.war

WEB UI usage 
	
	http://localhost:8080
	
Or run in your browser [http://localhost:8080/](http://localhost:8080/ "http://localhost:8080/")

REST API usage 
	
	http://localhost:8080/api/changes/{stat}:{end}
	
exmaple request

	curl http://localhost:8080/api/changes/2019-01-20:2019-01-23

Or run in your browser [http://localhost:8080/api/changes/2019-01-20:2019-01-23](http://localhost:8080/api/changes/2019-01-20:2019-01-23 "http://localhost:8080/api/changes/2019-01-20:2019-01-23") 

#### Frame works and libraries
Project uses Spring Boot Version 2.1.1  
For WEB UI used JSF with PrimeFaces 6.2 

#### External services
Currencies info and Rates provided from  SOAP service http://www.lb.lt/webservices/FxRates/  
SOAP interface sceleton generated with maven plugin maven-jaxb2-plugin.  
WSDL and schema xsd downloaded to src/main/resouces/schema directory.  
File binding.xjb used to correct some issue with provided schema  
Generated source files placed in package  lt.mazgis.hometasks.currencyrates.ratesprovider.lb.dao 

#### Project structure
Package __lt.mazgis.hometasks.currencyrates.ratesprovider.lb__ locates calsses required for communication with external SOAP service.
  
Class __LBFxRatesClient__ provides simple client for SOAP methods access.
 
Class __RateProviderLBFxRatesAdapter__ implements buisnes logic. At the moment is contains single method to provide currencies reates changes on given date range. 

Package __lt.mazgis.hometasks.currencyrates.view__ contains JSF bean for WEB UI. 
JSF page templates placed in directory __src/main/resources/META-INF/resources__.
 
Package __lt.mazgis.hometasks.currencyrates.rest__ contains simple RESTfull service provider

#### Configuration files
Spring Boot configuration file  __src/main/resources/application.properties__
WEB server port can be changes in file __src/main/resources/application.properties__ property `server.port` 
External Currency Rates url can be changes in file __src/main/resources/config.properties__ property `lb.fxrate.uri`

#### Tests
Project contains JUnit test for :

*   SAOP Service testing __LBFxRatesSoapMethosTests__
*   Currency Rates Provider (rates changes logic) testing  __CurrencyRatesProviderTests__
*	REST service integration test __RatesRestMethosTests__
*	Some Mock test __RatesProviderMockTest__
